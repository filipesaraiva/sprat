/***************************************************************************
 *   Copyright (C) 2013 by Filipe Saraiva <filipe@kde.org>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .        *
 ***************************************************************************/

#ifndef SENTECESDOCK_H
#define SENTECESDOCK_H

#include <KTextEdit>

#include <QDockWidget>
#include <QTreeWidget>

class SentencesDock : public QDockWidget
{
    Q_OBJECT

    public:
        SentencesDock();
        ~SentencesDock();
        
        QTreeWidget* getSentences();
        void sendSentencesToSentenceFilter();
        
    private:
        void setupDockElements();
        
        KTextEdit *m_sentencetext;
        KTextEdit *m_sentenceexample;
        
        QWidget *m_dockelements;
        QTreeWidget *m_sentences;
        QTreeWidget *m_sentenceshidden;
        
    signals:
        void sectionFound(QString);
        void restartFilters();
        
    public slots:
        void hideSection(QString);
        void showSection(QString);
        
    private slots:
        void filteringSentences(QString filter);
        void showSentenceText(QTreeWidgetItem *item);
        void showSentenceExample(QTreeWidgetItem *item);
};

#endif
