/***************************************************************************
 *   Copyright (C) 2013 by Filipe Saraiva <filipe@kde.org>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .        *
 ***************************************************************************/

#include "mainwindow.h"

#include "enums/itemtype.h"
#include "enums/sentencecomponent.h"
#include "enums/statusbaritem.h"

#include <QApplication>
#include <QAction>
#include <KLocalizedString>
#include <KActionCollection>
#include <KStandardAction>
#include <QFileDialog>
#include <KMessageBox>
#include <QSaveFile>
#include <KToolBar>
#include <KXMLGUIFactory>
#include <QStatusBar>
#include <QUrl>

#include <KTextEditor/Document>
#include <KTextEditor/View>
#include <KTextEditor/Editor>
#include <KTextEditor/ModificationInterface>
#include <KTextEditor/ConfigInterface>

#include <QDir>
#include <QTextStream>
#include <QTreeWidgetItem>
#include <QSettings>

MainWindow::MainWindow(QWidget *)
{
    KTextEditor::Editor *editor = KTextEditor::Editor::instance();

    if(!editor){

        KMessageBox::error(this, i18n("A KDE text-editor component could not be found;\n"
            "please check your KDE installation."));
        
        exit(1);
    }
    
    m_document = editor->createDocument(nullptr);
    m_view = qobject_cast<KTextEditor::View*>(m_document->createView(this));
    
    qobject_cast<KTextEditor::ModificationInterface *>(m_document)->setModifiedOnDiskWarning(true);
    
    connect(m_document, SIGNAL(modifiedChanged(KTextEditor::Document*)),
            this, SLOT(updateFileName()));
    
    connect(m_document, SIGNAL(documentUrlChanged(KTextEditor::Document*)),
            this, SLOT(updateFileName()));
    
    updateFileName();

    setCentralWidget(m_view);
    
    setDockOptions(QMainWindow::AnimatedDocks    |
                   QMainWindow::AllowTabbedDocks |
                   QMainWindow::VerticalTabs);
    
    m_sentencesdock = new SentencesDock();
    m_sentencesfilterdock = new SentencesFilterDock();
    
    addDockWidget(Qt::RightDockWidgetArea, m_sentencesdock);
    addDockWidget(Qt::RightDockWidgetArea, m_sentencesfilterdock);
    
    tabifyDockWidget(m_sentencesdock, m_sentencesfilterdock);
    m_sentencesdock->raise();
    
    connect(m_sentencesdock, SIGNAL(sectionFound(QString)),
        m_sentencesfilterdock, SLOT(addSectionItem(QString)));
    
    connect(m_sentencesdock, SIGNAL(restartFilters()),
        m_sentencesfilterdock, SLOT(sendSectionsStatus()));
    
    connect(m_sentencesfilterdock, SIGNAL(sectionDisabled(QString)),
        m_sentencesdock, SLOT(hideSection(QString)));
    
    connect(m_sentencesfilterdock, SIGNAL(sectionEnabled(QString)),
        m_sentencesdock, SLOT(showSection(QString)));
    
    m_sentencesdock->sendSentencesToSentenceFilter();

    setupActions();
    setupStatusBar();

    createShellGUI(true);

    guiFactory()->addClient(m_view);
    
    connect(m_document, SIGNAL(textChanged(KTextEditor::Document*)),
        this, SLOT(modifyStatusBarText()));
    
    connect(m_view, SIGNAL(selectionChanged(KTextEditor::View*)),
        this, SLOT(modifyStatusBarSelectedText()));
    
    connect(m_sentencesdock->getSentences(), SIGNAL(itemDoubleClicked(QTreeWidgetItem*,int)),
        this, SLOT(insertText(QTreeWidgetItem*)));
    
    readSettings();
    
    show();
}

MainWindow::~MainWindow()
{

}

void MainWindow::setupActions()
{
    KStandardAction::quit(this, SLOT(quit()), actionCollection());
    KStandardAction::open(this, SLOT(openFile()), actionCollection());
    KStandardAction::clear(this, SLOT(clear()), actionCollection());
    
    m_docksVisibility = new QAction(QIcon::fromTheme("view-right-close"), i18n("Hide Panels"), this);
    
    connect(m_docksVisibility, SIGNAL(triggered(bool)),
        this, SLOT(setDockVisibility()));
  
    toolBar()->addAction(m_docksVisibility);
}

void MainWindow::setupStatusBar()
{
    auto iface = qobject_cast<KTextEditor::ConfigInterface*>(m_view);

    if (iface) {
        iface->setConfigValue("word-count", true);
    }
}

void MainWindow::writeSettings()
{
    QSettings settings("sprat", "sprat");

    settings.beginGroup("MainWindow");
    settings.setValue("MainWindow/Size", size());
    settings.setValue("MainWindow/Pos", pos());
    settings.setValue("MainWindow/SentencesDockWidgetArea", dockWidgetArea(m_sentencesdock));
    settings.setValue("MainWindow/SentencesFilterDockWidgetArea", dockWidgetArea(m_sentencesfilterdock));
    settings.endGroup();

    settings.beginGroup("SentencesDock");
    settings.setValue("SentencesDock/Size", m_sentencesdock->size());
    settings.setValue("SentencesDock/Pos", m_sentencesdock->pos());
    settings.setValue("SentencesDock/Floating", m_sentencesdock->isFloating());
    
    if(m_sentencesdock->isVisible())
        settings.setValue("SentencesDock/Visible", true);
    else
        settings.setValue("SentencesDock/Visible", false);
    
    settings.endGroup();

    settings.beginGroup("SentencesFilterDock");
    settings.setValue("SentencesFilterDock/Size", m_sentencesfilterdock->size());
    settings.setValue("SentencesFilterDock/Pos", m_sentencesfilterdock->pos());
    settings.setValue("SentencesFilterDock/Floating", m_sentencesfilterdock->isFloating());
    
    if(m_sentencesfilterdock->isVisible())
        settings.setValue("SentencesFilterDock/Visible", true);
    else
        settings.setValue("SentencesFilterDock/Visible", false);
    
    settings.endGroup();

    settings.beginGroup("DockVisibility");
    settings.setValue("DockVisibility/Text", m_docksVisibility->text());
    settings.setValue("DockVisibility/Icon", m_docksVisibility->icon().name());
    settings.endGroup();
}

void MainWindow::readSettings()
{
    QSettings settings("sprat", "sprat");

    settings.beginGroup("MainWindow");
    resize(settings.value("MainWindow/Size", sizeHint()).toSize());
    move(settings.value("MainWindow/Pos", pos()).toPoint());
    addDockWidget(Qt::DockWidgetArea(settings.value("MainWindow/SentencesDockWidgetArea").toInt()), m_sentencesdock);
    addDockWidget(Qt::DockWidgetArea(settings.value("MainWindow/SentencesFilterDockWidgetArea").toInt()), m_sentencesfilterdock);
    settings.endGroup();

    settings.beginGroup("SentencesDock");
    m_sentencesdock->resize(settings.value("SentencesDock/Size", sizeHint()).toSize());
    m_sentencesdock->move(settings.value("SentencesDock/Pos", pos()).toPoint());
    m_sentencesdock->setVisible(settings.value("SentencesDock/Visible", true).toBool());
    bool sentencesDockVisibility = settings.value("SentencesDock/Visible", true).toBool();
    m_sentencesdock->setFloating(settings.value("SentencesDock/Floating", false).toBool());
    settings.endGroup();

    settings.beginGroup("SentencesFilterDock");
    m_sentencesfilterdock->resize(settings.value("SentencesFilterDock/Size", sizeHint()).toSize());
    m_sentencesfilterdock->move(settings.value("SentencesFilterDock/Pos", pos()).toPoint());
    m_sentencesfilterdock->setVisible(settings.value("SentencesFilterDock/Visible", true).toBool());
    m_sentencesfilterdock->setFloating(settings.value("SentencesFilterDock/Floating", false).toBool());
    settings.endGroup();

    settings.beginGroup("DockVisibility");
    m_docksVisibility->setText(settings.value("DockVisibility/Text", m_docksVisibility->text()).toString());
    
    if(settings.value("DockVisibility/Icon", m_docksVisibility->icon().name()).toString() == "view-right-new")
        m_docksVisibility->setIcon(QIcon::fromTheme("view-right-new"));
    else
        m_docksVisibility->setIcon(QIcon::fromTheme("view-right-close"));
    
    settings.endGroup();
    
    if(dockWidgetArea(m_sentencesdock) == dockWidgetArea(m_sentencesfilterdock)){
        
        if(m_sentencesdock->size() == m_sentencesfilterdock->size()){
            
            if(m_sentencesdock->pos().y() >= m_sentencesfilterdock->pos().y()){
                tabifyDockWidget(m_sentencesdock, m_sentencesfilterdock);
                m_sentencesdock->raise();
            } else {
                tabifyDockWidget(m_sentencesfilterdock, m_sentencesdock);
                m_sentencesfilterdock->raise();
            }
        
        } else {
            
            if(m_sentencesfilterdock->pos().y() < m_sentencesdock->pos().y()){
                int sentencesDockArea = dockWidgetArea(m_sentencesdock);
                
                removeDockWidget(m_sentencesdock);
                addDockWidget(Qt::DockWidgetArea(sentencesDockArea), m_sentencesdock);
                m_sentencesdock->setVisible(sentencesDockVisibility);
            }            
        }
    }
    
}

bool MainWindow::queryClose()
{
    bool documentSaved = false;
    
    if(m_document->isModified()){
        documentSaved = m_document->queryClose();
    } else {
        documentSaved = true;
    }
    
    if(documentSaved){
        writeSettings();
    }
    
    return documentSaved;
}
 
void MainWindow::clear()
{
    m_document->clear();
}

void MainWindow::openFile()
{
    m_view->document()->openUrl(QFileDialog::getOpenFileName());
}

void MainWindow::insertText(QTreeWidgetItem *item)
{
    if(item->type() == ItemType::Sentence){
        m_document->insertText(m_view->cursorPosition(), item->text(SentenceComponent::Text) + " ");
    }
}

void MainWindow::setDockVisibility()
{
    if(m_sentencesdock->isVisible()){
        m_sentencesdock->setVisible(false);
        m_sentencesfilterdock->setVisible(false);
        
        m_docksVisibility->setIcon(QIcon::fromTheme("view-right-new"));
        m_docksVisibility->setText(i18n("Show Panels"));
    } else {
        m_sentencesdock->setVisible(true);
        m_sentencesfilterdock->setVisible(true);
        
        m_docksVisibility->setIcon(QIcon::fromTheme("view-right-close"));
        m_docksVisibility->setText(i18n("Hide Panels"));
    }
}

void MainWindow::updateFileName()
{
    QString fileName = m_document->url().fileName();
    bool modified = m_document->isModified();
    
    if (fileName.isEmpty()){
        setCaption(i18n("Untitled"), modified);
    } else {
        setCaption(i18n("%1", fileName), modified);
    }
}
