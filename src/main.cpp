/***************************************************************************
 *   Copyright (C) 2013 by Filipe Saraiva <filipe@kde.org>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .        *
 ***************************************************************************/

#include "mainwindow.h"

#include <QApplication>
#include <KAboutData>
#include <QCommandLineParser>
#include <KLocalizedString>

static const char description[] =
    I18N_NOOP("A assistant to write scientific paper drafts");

static const char version[] = "0.1";

int main(int argc, char **argv)
{
    QApplication app(argc, argv);

    KAboutData about(QLatin1String("sprat"),
                     QLatin1String("Sprat"),
                     version,
                     i18n(description),
                     KAboutLicense::GPL_V3,
                     i18n("(C) 2013, Filipe Saraiva"),
                     QString(),
                     QString());

    about.addAuthor(i18n("Filipe Saraiva"), i18n("Original creator"), QLatin1String("filipe@kde.org"),
                     QLatin1String("http://filipesaraiva.info/"));

    QCommandLineParser parser;
    about.setupCommandLine(&parser);

    parser.addPositionalArgument(QStringLiteral("urls"), i18n("Documents to open."), i18n("[urls...]"));
    parser.process(app);

    MainWindow *window = new MainWindow();
    window->show();
    
    return app.exec();
}
