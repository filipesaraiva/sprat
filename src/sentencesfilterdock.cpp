/***************************************************************************
 *   Copyright (C) 2013 by Filipe Saraiva <filipe@kde.org>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .        *
 ***************************************************************************/

#include "sentencesfilterdock.h"

#include <QIcon>
#include <KLocalizedString>

#include <QCheckBox>
#include <QString>
#include <QVBoxLayout>

SentencesFilterDock::SentencesFilterDock()
{   
    setObjectName("SentencesFilterDock");
    setWindowTitle(i18n("Sentences Filter"));
    setMinimumWidth(300);
    
    setFeatures(QDockWidget::DockWidgetMovable | QDockWidget::DockWidgetFloatable);
    setAllowedAreas(Qt::LeftDockWidgetArea | Qt::RightDockWidgetArea);
    
    m_sectionbox = new QGroupBox(i18n("Sections Enabled"));
    m_sectionboxlayout = new QVBoxLayout();
    m_sectionlist = new QList<QCheckBox*>();
    
    setupDockElements();
    
    setWidget(m_dockelements);
}

SentencesFilterDock::~SentencesFilterDock()
{

}

void SentencesFilterDock::setupDockElements()
{   
    QVBoxLayout *layout = new QVBoxLayout;
    
    layout->setAlignment(Qt::AlignTop);
    
    m_sectionbox->setLayout(m_sectionboxlayout);
    layout->addWidget(m_sectionbox);
    
    m_dockelements = new QWidget;
    m_dockelements->setLayout(layout);
    
}

void SentencesFilterDock::addSectionItem(QString item)
{
    QCheckBox *check = new QCheckBox(item);
    check->setCheckState(Qt::Checked);
    check->setIcon(QIcon::fromTheme("file-manager"));
    
    connect(check, SIGNAL(stateChanged(int)),
        this, SLOT(sendSectionsStatus()));
    
    m_sectionlist->append(check);
    m_sectionboxlayout->addWidget(check);
}

void SentencesFilterDock::sendSectionsStatus()
{
    QList<QCheckBox*>::iterator it;
    
    for(it = m_sectionlist->begin(); it != m_sectionlist->end(); it++){
        if((*it)->checkState() == Qt::Unchecked)
            emit(sectionDisabled((*it)->text()));
        
        if((*it)->checkState() == Qt::Checked)
            emit(sectionEnabled((*it)->text()));
    }
    
}
