/***************************************************************************
 *   Copyright (C) 2013 by Filipe Saraiva <filipe@kde.org>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .        *
 ***************************************************************************/

#ifndef SENTENCESFILTERDOCK_H
#define SENTENCESFILTERDOCK_H

#include <QCheckBox>
#include <QDockWidget>
#include <QGroupBox>
#include <QList>
#include <QVBoxLayout>

class SentencesFilterDock : public QDockWidget
{
    Q_OBJECT

    public:
        SentencesFilterDock();
        ~SentencesFilterDock();
        
    private:
        void setupDockElements();
        
        QGroupBox *m_sectionbox;
        QVBoxLayout *m_sectionboxlayout;
        QList<QCheckBox*> *m_sectionlist;
        
        QWidget *m_dockelements;
        
    signals:
        void sectionDisabled(QString);
        void sectionEnabled(QString);
        
    public slots:
        void addSectionItem(QString item);
        void sendSectionsStatus();

};

#endif
