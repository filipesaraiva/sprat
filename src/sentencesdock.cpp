/***************************************************************************
 *   Copyright (C) 2013 by Filipe Saraiva <filipe@kde.org>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .        *
 ***************************************************************************/

#include "sentencesdock.h"

#include "enums/itemtype.h"
#include "enums/sentencecomponent.h"
#include "sentencesfilterdock.h"

#include <KLocalizedString>
#include <KLineEdit>
#include <KMessageBox>
#include <QStandardPaths>

#include <QFile>
#include <QLabel>
#include <QTreeWidgetItemIterator>
#include <QVBoxLayout>
#include <QXmlStreamReader>

SentencesDock::SentencesDock()
{
    
    setWindowTitle(i18n("Compendium"));
    setMinimumWidth(300);
    
    setFeatures(QDockWidget::DockWidgetMovable | QDockWidget::DockWidgetFloatable);
    setAllowedAreas(Qt::LeftDockWidgetArea | Qt::RightDockWidgetArea);
    
    setupDockElements();
    
    m_sentenceshidden = new QTreeWidget();
    
    setWidget(m_dockelements);
}

SentencesDock::~SentencesDock()
{

}

QTreeWidget* SentencesDock::getSentences()
{
    return m_sentences;
}

void SentencesDock::sendSentencesToSentenceFilter()
{
    QTreeWidgetItemIterator it(m_sentences);
     while(*it){
         if((*it)->type() == ItemType::Section)
             emit(sectionFound((*it)->text(SentenceComponent::Text)));
         it++;
     }
}

void SentencesDock::setupDockElements()
{   
    m_sentences = new QTreeWidget();
    m_sentences->setColumnCount(1);
    m_sentences->setMouseTracking(true);
    m_sentences->setHeaderLabel(i18n("Sentences"));
    
    KLineEdit *filterSentences = new KLineEdit();
    filterSentences->setPlaceholderText(i18n("Sentences filter"));
    
    QFile file(QLatin1String("/home/filipe/.sprat/techsaurus.xml"));

    if(!file.open(QIODevice::ReadOnly))
    {
        KMessageBox::error(this, i18n("A saurus component could not be found;\n"
            "please check your Sprat installation."));
        
        return;
    }
    
    QList<QTreeWidgetItem *> items;
    QXmlStreamReader xml(&file);

    while(xml.readNextStartElement()){

        QStringRef name = xml.name();

        if(name == "section"){
            
            QXmlStreamAttributes sectionAtr = xml.attributes();
            QStringRef sectionType = sectionAtr.value("type");
            
            QTreeWidgetItem *section = new QTreeWidgetItem(QStringList(sectionType.toString()),
                                                           ItemType::Section);
            
            section->setIcon(SentenceComponent::Text, QIcon::fromTheme("folder"));
            items.append(section);

            while(xml.readNextStartElement()){

                QStringRef name = xml.name();

                if(name == "subsection"){
                    QXmlStreamAttributes subsectionAtr = xml.attributes();
                    QStringRef subsectionType = subsectionAtr.value("type");
                    
                    QTreeWidgetItem *subsection = new QTreeWidgetItem(section,
                                                                   QStringList(subsectionType.toString()),
                                                                   ItemType::Subsection);
                    
                    subsection->setIcon(SentenceComponent::Text, QIcon::fromTheme("text-x-generic-template"));
                    items.append(subsection);

                    while(xml.readNextStartElement()){

                        xml.readNextStartElement();
                        QString text = xml.readElementText();

                        xml.readNextStartElement();
                        QString example = xml.readElementText();
                        
                        QStringList sentenceComponents;
                        sentenceComponents << text << example;
                        
                        QTreeWidgetItem *sentence = new QTreeWidgetItem(subsection,
                                                                        sentenceComponents,
                                                                        ItemType::Sentence);
                        
                        sentence->setToolTip(SentenceComponent::Text, text);
                        
                        sentence->setIcon(SentenceComponent::Text, QIcon::fromTheme("document-edit-verify"));
                        items.append(sentence);

                        xml.skipCurrentElement();
                    }
                }
            }
        }
    }
    
    m_sentences->addTopLevelItems(items);
    
    QLabel *sentenceTextTitle = new QLabel();
    sentenceTextTitle->setText(i18n("Sentence text"));
    
    m_sentencetext = new KTextEdit();
    m_sentencetext->setReadOnly(true);
    m_sentencetext->setMaximumHeight(100);
    
    QLabel *sentenceExampleTitle = new QLabel();
    sentenceExampleTitle->setText(i18n("Sentence example"));
    
    m_sentenceexample = new KTextEdit();
    m_sentenceexample->setReadOnly(true);
    m_sentenceexample->setMaximumHeight(100);
    
    QVBoxLayout *layout = new QVBoxLayout;
    layout->addWidget(filterSentences);
    layout->addWidget(m_sentences);
    layout->addWidget(sentenceTextTitle);
    layout->addWidget(m_sentencetext);
    layout->addWidget(sentenceExampleTitle);
    layout->addWidget(m_sentenceexample);
    
    m_dockelements = new QWidget;
    m_dockelements->setLayout(layout);
    
    connect(filterSentences, SIGNAL(textChanged(QString)),
        this, SLOT(filteringSentences(QString)));
    
    connect(m_sentences, SIGNAL(itemClicked(QTreeWidgetItem*,int)),
        this, SLOT(showSentenceText(QTreeWidgetItem*)));
    
    connect(m_sentences, SIGNAL(itemClicked(QTreeWidgetItem*,int)),
        this, SLOT(showSentenceExample(QTreeWidgetItem*)));
}

void SentencesDock::hideSection(QString section)
{
    /**
     * QString section came with '&' symbol in begin
     */
    section.replace("&","");

    QTreeWidgetItemIterator it(m_sentences);
    while(*it){
        if(((*it)->type() == ItemType::Section) &&
            ((*it)->text(SentenceComponent::Text) == section)){
            
            (*it)->setHidden(true);
            (*it)->setExpanded(false);
        }
        
        if((*it)->type() != ItemType::Section){
           (*it)->setHidden(false);
           (*it)->setExpanded(false);
        }
        
        it++;
    }
}

void SentencesDock::showSection(QString section)
{
    /**
     * QString section came with '&' symbol in begin
     */
    section.replace("&","");

    QTreeWidgetItemIterator it(m_sentences);
     while(*it){
         if(((*it)->type() == ItemType::Section) &&
             ((*it)->text(SentenceComponent::Text) == section)){
             
             (*it)->setHidden(false);
             (*it)->setExpanded(false);
         }
         
         if((*it)->type() != ItemType::Section){
           (*it)->setHidden(false);
           (*it)->setExpanded(false);
        }
         
         it++;
     }
}

void SentencesDock::filteringSentences(QString filter)
{
    QTreeWidgetItemIterator itemIterator(m_sentences);
    
    emit restartFilters();

    if(!filter.isEmpty()){
        while(*itemIterator){
            if(((*itemIterator)->type() == ItemType::Section) &&
               ((*itemIterator)->isHidden())){
                
                    itemIterator++;
                    
                    while(*itemIterator){
                        if((*itemIterator)->type() != ItemType::Section){
                            itemIterator++;
                        } else {
                            break;
                        }
                    }
                    
                    continue;
            }
            
            (*itemIterator)->setHidden(true);
            
            if(((*itemIterator)->type() == ItemType::Sentence) &&
               ((*itemIterator)->text(SentenceComponent::Text).contains(filter, Qt::CaseInsensitive))){
                
                (*itemIterator)->setHidden(false);
                (*itemIterator)->parent()->setHidden(false);
                (*itemIterator)->parent()->parent()->setHidden(false);
                (*itemIterator)->parent()->setExpanded(true);
                (*itemIterator)->parent()->parent()->setExpanded(true);
            }
            itemIterator++;
            
        }
    } else {
        /*
         * Empty 'else' mandatory to show
         * sentences unexpanded
         */
    }
        
}

void SentencesDock::showSentenceText(QTreeWidgetItem *item)
{
    if(item->type() == ItemType::Sentence){
        m_sentencetext->setPlainText(item->text(SentenceComponent::Text));
    }
}

void SentencesDock::showSentenceExample(QTreeWidgetItem *item)
{
    if(item->type() == ItemType::Sentence){
        m_sentenceexample->setPlainText(item->text(SentenceComponent::Example));
    }
}
