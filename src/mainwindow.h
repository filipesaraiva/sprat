/***************************************************************************
 *   Copyright (C) 2013 by Filipe Saraiva <filipe@kde.org>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .        *
 ***************************************************************************/

#ifndef SPRAT_H
#define SPRAT_H

#include "sentencesdock.h"
#include "sentencesfilterdock.h"

#include <KParts/MainWindow>
#include <QtGui/QKeyEvent>

#include <QDockWidget>
#include <QWidget>

namespace KTextEditor
{
    class Document;
    class View;
}

class QAction;

class MainWindow : public KParts::MainWindow
{
    Q_OBJECT

    public:
        MainWindow(QWidget *parent=nullptr);
        ~MainWindow();
        
    private:
        void setupActions();
        void setupStatusBar();
        void writeSettings();
        void readSettings();
        
        virtual bool queryClose();
        
        KTextEditor::View *m_view;
        KTextEditor::Document *m_document;
        
        QAction *m_docksVisibility;
        
        SentencesDock *m_sentencesdock;
        SentencesFilterDock *m_sentencesfilterdock; 
        
    private slots:
        void clear();
        void openFile();
        void insertText(QTreeWidgetItem *item);
        void setDockVisibility();
        void updateFileName();
};

#endif
