cmake_minimum_required (VERSION 3.5 FATAL_ERROR)

project(sprat)

find_package(Qt5 5.6.0 CONFIG REQUIRED
    Core
    Widgets
    XmlPatterns)

find_package(ECM 5.15.0 CONFIG REQUIRED)
set(CMAKE_MODULE_PATH ${ECM_MODULE_PATH} ${ECM_KDE_MODULE_DIR})
set(CMAKE_AUTOMOC ON)

find_package(KF5 5.15 REQUIRED
    CoreAddons
    WidgetsAddons
    TextEditor)

include(ECMInstallIcons)
include(KDEInstallDirs)

#add_subdirectory(doc)
add_subdirectory(src)
add_subdirectory(icons)
#add_subdirectory(saurus)
